import plotador
import testatodos

def main():
    cloud_aleat = False
    cloud_dist = 0
    cloudlet_data = [float(i) for i in input("Nessa ordem separado por espacos: RAM cloudlet, HD cloudlet: ").split()]
    cpu = [float(i) for i in input("Nessa ordem separado por espacos: uso de CPU minimo das vms, uso de CPU maximo das vms: ").split()]
    ram = [float(i) for i in input("Nessa ordem separado por espacos: uso de RAM minimo das vms, uso de RAM maximo das vms: ").split()]
    hd = [float(i) for i in input("Nessa ordem separado por espacos: uso de HD minimo das vms, uso de HD maximo das vms: ").split()]
    casos_teste = int(input("Quantos casos teste deseja para cada valor? "))
    number = int(input("VMs por teste"))
    cpumin = int(input("Valor minimo de CPU: "))
    cpumax = int(input("Valor maximo de CPU: "))
    inter = int(input("Intervalo entre Valores de CPU: "))
    tipo = int(input("Tipo de distribuição (0-normal, 1-normal logaritmica, 2-uniforme): "))

    print("\n\n\n\n\n")
    results = []
    for i in range(cpumin,cpumax+inter,inter):
        results.append([i,testatodos.test([i]+cloudlet_data,cpu,ram,hd,number,tipo,casos_teste, cloud_aleat, cloud_dist)])
        print("{0:.2f}".format(100*(i-cpumin+inter)/(cpumax+inter-cpumin))+"%")
    plotador.plotar(results,1, "Varia CPU")

if __name__ == "__main__":
    main()
