import matplotlib.pyplot as plt

def plotar(data, index, nome = "Graph"):
    ys = []
    names = []
    x = []
    for i in data[0][1]:
        ys.append([])
        names.append(i[0])
    for point in data:
        x.append(point[0])
        for i in range(4):
            ys[i].append(point[1][i][index])
    for i in range(len(names)):
        plt.plot(x,ys[i],label = names[i])
    #plt.plot(x,x,label = "test")
    plt.xlabel('x - axis')
    plt.ylabel('y - axis')
    plt.title(nome)
    plt.legend()
    plt.show()
