import Cloudlet
import VM
import dataset_generator

def get_dataset():
    return dataset_generator.new_set(0.1,2,0.1,2,1,30,100,1000)

def get_cloudlet_data():
    return [100,100,100]

def alloc(dataset, cloudlet_data):
    vm_list = VM.build_vms(dataset)
    cloudlet = Cloudlet.Cloudlet(cloudlet_data[0],cloudlet_data[1], cloudlet_data[2])
    while len(vm_list) > 0:
        max_value = vm_list[0].calc_value_2(cloudlet)
        best_vm = vm_list[0]
        for i in range(1,len(vm_list)):
            temp = vm_list[i].calc_value_2(cloudlet)
            if temp > max_value:
                best_vm = vm_list[i]
                max_value = temp
        if cloudlet.add_vm(best_vm,max_value):
            vm_list.remove(best_vm)
        else:
            break
    return cloudlet.vm_ammount


if __name__ == '__main__':
    dataset = get_dataset()
    print(len(dataset))
    cloudlet_data = get_cloudlet_data()
    
    print("Foram alocados: "+str(alloc(dataset,cloudlet_data))+" maquinas virtuais na cloudlet")
