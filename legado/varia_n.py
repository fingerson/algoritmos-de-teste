import plotador
import testatodos

def main():
    cloud_aleat = False
    if int(input("Cloudlet aleatoria? (s = 1/n = 0)")) == 1:
        cloudlet_data = [float(i) for i in input("Nessa ordem separado por espacos: CPU  minimo cloudlet, RAM  minimo cloudlet, HD minimo cloudlet: ").split()]
        cloudlet_data += [float(i) for i in input("Nessa ordem separado por espacos: CPU  maximo cloudlet, RAM  maximo cloudlet, HD maximo cloudlet: ").split()]
        cloud_aleat = True
        cloud_dist = int(input("Tipo de distribuição (0-normal, 1-normal logaritmica, 2-uniforme): "))
    else:
        cloud_aleat = False
        cloud_dist = 0
        cloudlet_data = [float(i) for i in input("Nessa ordem separado por espacos: CPU cloudlet, RAM cloudlet, HD cloudlet: ").split()]
    cpu = [float(i) for i in input("Nessa ordem separado por espacos: uso de CPU minimo das vms, uso de CPU maximo das vms: ").split()]
    ram = [float(i) for i in input("Nessa ordem separado por espacos: uso de RAM minimo das vms, uso de RAM maximo das vms: ").split()]
    hd = [float(i) for i in input("Nessa ordem separado por espacos: uso de HD minimo das vms, uso de HD maximo das vms: ").split()]
    casos_teste = int(input("Quantos casos teste deseja para cada valor de n? "))
    number = int(input("Numero minimo de maquinas virtuais por teste: "))
    number2 = int(input("Numero maximo de maquinas virtuais por teste: "))
    inter = int(input("Intervalo entre numeros de VMs: "))
    tipo = int(input("Tipo de distribuição (0-normal, 1-normal logaritmica, 2-uniforme): "))

    print("\n\n\n\n\n")
    results = []
    for i in range(number,number2+inter,inter):
        results.append([i,testatodos.test(cloudlet_data,cpu,ram,hd,i,tipo,casos_teste, cloud_aleat, cloud_dist)])
        print("{0:.2f}".format(100*(i-number)/(number2+inter-number))+"%")
    plotador.plotar(results,1)

if __name__ == "__main__":
    main()
