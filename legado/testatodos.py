import dataset_generator
import Cloudlet
import VM
import alg0_1
import alg0_2
import alg0_alternativo
import alg0_alternativo_2
import time
import numpy

def test(cloudlet_data_raw,cpu,ram,hd,number,dist,casos_teste, random_cloudlet = False, cloudlet_dist = 2):
    n_alg0_1 = []
    n_alg0_2 = []
    n_alg0_alt = []
    n_alg0_alt2 = []
    t_0_1 = 0
    t_0_2 = 0
    t_a_1 = 0
    t_a_2 = 0

    for i in range(casos_teste):
        if random_cloudlet:
            cloudlet_data = dataset_generator.get_random_cloudlet(cloudlet_data_raw,cloudlet_dist)
        else:
            cloudlet_data = cloudlet_data_raw
        dataset = dataset_generator.new_set(cpu[0],cpu[1],ram[0],ram[1],hd[0],hd[1], number, dist)
        ct = time.time()
        n_alg0_1.append(alg0_1.alloc(dataset,cloudlet_data))
        t_0_1+=time.time()-ct
        ct = time.time()
        n_alg0_2.append(alg0_2.alloc(dataset,cloudlet_data))
        t_0_2+=time.time()-ct
        ct = time.time()
        n_alg0_alt.append(alg0_alternativo.alloc(dataset,cloudlet_data))
        t_a_1+=time.time()-ct
        ct = time.time()
        n_alg0_alt2.append(alg0_alternativo_2.alloc(dataset,cloudlet_data))
        t_a_2+=time.time()-ct
    res_0 = ["Alg padrão, calculo 1, ",sum(n_alg0_1)/casos_teste,numpy.std(n_alg0_1),t_0_1/casos_teste]
    res_1 = ["Alg padrão, calculo 2, ",sum(n_alg0_2)/casos_teste,numpy.std(n_alg0_2),t_0_2/casos_teste]
    res_a = ["Alg próprio, calculo 1, ",sum(n_alg0_alt)/casos_teste,numpy.std(n_alg0_alt),t_a_1/casos_teste]
    res_a2 = ["Alg próprio, calculo 2, ",sum(n_alg0_alt2)/casos_teste,numpy.std(n_alg0_alt2),t_a_2/casos_teste]
    return [res_0,res_1,res_a,res_a2]

def main():
    cloudlet_data = [float(i) for i in input("Nessa ordem separado por espacos: CPU cloudlet, RAM cloudlet, HD cloudlet: ").split()]

    cpu = [float(i) for i in input("Nessa ordem separado por espacos: uso de CPU minimo das vms, uso de CPU maximo das vms: ").split()]
    ram = [float(i) for i in input("Nessa ordem separado por espacos: uso de RAM minimo das vms, uso de RAM maximo das vms: ").split()]
    hd = [float(i) for i in input("Nessa ordem separado por espacos: uso de HD minimo das vms, uso de HD maximo das vms: ").split()]
    number = int(input("Numero de maquinas virtuais por teste: "))
    tipo = int(input("Tipo de distribuição (0-normal, 1-normal logaritmica, 2-uniforme): "))
    casos_teste = int(input("Quantos casos teste deseja? "))
    print("\n\n\n\n\n")
    results = test(cloudlet_data,cpu,ram,hd,number,tipo,casos_teste)
    for r in results:
        print(r[0]+"media mvs alocadas: "+str(r[1])+", desvio padrão: "+str(r[2])+", media tempo: "+str(r[3])+" ms\n")


if __name__ == "__main__":
    main()
