import Cloudlet
import VM
import dataset_generator
import random

def get_random(min, max):
    frac = random.random()
    frac = frac*(max-min) + min
    return frac

def get_dataset():
    return dataset_generator.new_set(400,4000,0.5,4,5,50,50,1000)

def get_cloudlet_data():
    return [get_random(30000,50000),get_random(20,80),get_random(1000,10000)]

def alloc(dataset, cloudlet_data):
    vm_list = VM.build_vms(dataset)
    cloudlet = Cloudlet.Cloudlet(cloudlet_data[0],cloudlet_data[1], cloudlet_data[2])
    vm_value_list = []
    for vir_mac in vm_list:
        vm_value_list.append((vir_mac,vir_mac.calc_value_2(cloudlet)))
    vm_value_list.sort(key=lambda tup: tup[1])
    vm_value_list.reverse()
    while(len(vm_value_list) > 0 and cloudlet.add_vm(vm_value_list[0][0],vm_value_list[0][1])):
        vm_value_list.pop(0)
    return cloudlet.vm_ammount

if __name__ == '__main__':
    dataset = get_dataset()
    print(len(dataset))
    cloudlet_data = get_cloudlet_data()
    
    print("Foram alocados: "+str(alloc(dataset,cloudlet_data))+" maquinas virtuais na cloudlet")
