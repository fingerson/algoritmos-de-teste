class Cloudlet:
    def __init__(self, CPU, RAM, HD):
        self.max_cpu = CPU
        self.cpu = CPU
        self.max_ram = RAM
        self.ram = RAM
        self.max_hd = HD
        self.hd = HD
        self.vms = []
        self.value = 0
        self.vm_ammount = 0

    def add_vm(self,vm):
        if self.ram-vm.ram < 0 or self.cpu-vm.cpu < 0 or self.hd-vm.hd < 0:
            return False
        self.vms.append(vm)
        self.ram-=vm.ram
        self.cpu-=vm.cpu
        self.hd-=vm.hd
        self.value += vm.value
        self.vm_ammount += 1
        return True

    def remove_vm(self, vm):
        if vm in self.vms:
            self.vms.remove(vm)
            self.ram+=vm.ram
            self.cpu+=vm.cpu
            self.hd+=vm.hd
            self.value -= vm.value
            self.vm_ammount -= 1
            return True
        return False
