class Virtual_Machine:
    def __init__(self,CPU,RAM,HD,delay = 1):
        self.cpu = CPU
        self.ram = RAM
        self.hd = HD
        self.delay_max = delay
        self.value = 0

    def calc_value_1(self,cloudlet, distance = -1):
        hd_frac = self.hd/(0.0000001+cloudlet.hd)
        ram_frac = self.ram/(0.0000001+cloudlet.ram)
        cpu_frac = self.cpu/(0.0000001+cloudlet.cpu)
        value = -distance/(0.0000001+ram_frac+hd_frac+cpu_frac)
        return value

    def calc_value_2(self,cloudlet, distance = -1):
        hd_frac = self.hd/(0.0000001+cloudlet.hd)
        ram_frac = self.ram/(0.0000001+cloudlet.ram)
        cpu_frac = self.cpu/(0.0000001+cloudlet.cpu)
        value = -distance/(0.0000001+max(ram_frac,hd_frac,cpu_frac))
        return value

def build_vms(dataset):
    vms = []
    for line in dataset:
        if len(line) == 3:
            new_vm = Virtual_Machine(line[0],line[1],line[2])
        else:
            new_vm = Virtual_Machine(line[0],line[1],line[2],line[3])
        vms.append(new_vm)
    return vms
