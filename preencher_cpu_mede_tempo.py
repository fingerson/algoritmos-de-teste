import testes_paradigmas.busca_exaustiva.alg_preencher_cpu as alg_preencher_exaustivo
import testes_paradigmas.prog_dinamica.preenche_cpu_din as alg_preencher_dinamico
import testes_paradigmas.prog_dinamica_sem_comb.preenche_cpu_din as alg_preencher_dinamico_sc
import testes_paradigmas.preenche_como_der.alg_controle as alg_preencher_linear
import testes_paradigmas.calculo_valor_indiv.alg_calcula_valor as alg_preencher_calcula
import Cloudlet
import VM
import dataset_generator
import random
import time

def get_random(min, max):
    frac = random.random()
    frac = frac*(max-min) + min
    return frac

def get_dataset(n = 1000):
    return dataset_generator.new_set(10,100,10,100,10,100,n,2)

def get_cloudlet_data():
    return [get_random(100,500),get_random(100,500),get_random(100,500)]

n_cloudlets = int(input())
n_vms = int(input())
cloudlet_data = [get_cloudlet_data() for i in range(n_cloudlets)]
dataset = get_dataset(n_vms)
res = alg_preencher_linear.alloc(dataset[0:n_vms], cloudlet_data)
print("%.5f, " %res["time"], end = "")
print(res["fraction"])
print("linear")
res = alg_preencher_calcula.alloc(dataset[0:n_vms], cloudlet_data)
print("%.5f, " %res["time"], end = "")
print(res["fraction"])
print("calcula_valor")
res = alg_preencher_dinamico.alloc(dataset[0:n_vms], cloudlet_data)
print("%.5f, " %res["time"], end = "")
print(res["fraction"])
print("dinamico")
res = alg_preencher_dinamico_sc.alloc(dataset[0:n_vms], cloudlet_data)
print("%.5f, " %res["time"], end = "")
print(res["fraction"])
print("dinamico sc")
#for i in range(1, n_vms):
#    res = alg_preencher_linear.alloc(dataset[0:i], cloudlet_data)
#    print("%.5f, " %res["time"], end = "")
#    print(res["fraction"])
#print("linear")
#for i in range(1, n_vms):
#    res = alg_preencher_dinamico.alloc(dataset[0:i], cloudlet_data)
#    print("%.5f, " %res["time"], end = "")
#    print(res["fraction"])
#print("dinamico")
#for i in range(1, n_vms):
#    res = alg_preencher_dinamico_sc.alloc(dataset[0:i], cloudlet_data)
#    print("%.5f, " %res["time"], end = "")
#    print(res["fraction"])
#print("dinamico sc")
#for i in range(1, n_vms):
#    res = alg_preencher_calcula.alloc(dataset[0:i], cloudlet_data)
#    print("%.5f, " %res["time"], end = "")
#    print(res["fraction"])
#print("calcula_valor")
#for i in range(1, n_vms):
#    res = alg_preencher_exaustivo.alloc(dataset[0:i], cloudlet_data)
#    print("%.5f, " %res["time"], end = "")
#    print(res["fraction"])
#print("exaustivo")
