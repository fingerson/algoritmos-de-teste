import time

def le_lists(a,b):
    ist = True
    for i in range(min(len(a),len(b))):
        ist = ist and (a[i] <= b[i])
    return ist

def sum_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] + b[i])
    return c

def div_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        if b[i] != 0 :
            c.append(a[i]/b[i])
        else:
            c.append(0)
    return c

def preenche(cloudlets, items):


    value_list = []

    for item in items:
        for i in range(len(cloudlets)):
            item_val = item[0]/(sum(div_lists(item[1],cloudlets[i]))+1)
            cloud_index = i
            value_list.append((item_val,cloud_index,item[1]))
    value_list.sort()
    value_list.reverse()

    vms = []
    valores = 0
    utilizado = [[0 for i in cloudlets[0]] for j in cloudlets]
    for vm_value in value_list:
        _, cloud_index, vm = vm_value
        if vm in vms:
            continue
        temp = sum_lists(utilizado[cloud_index],vm)
        if le_lists(temp,cloudlets[cloud_index]):
            utilizado[cloud_index] = temp
            vms.append(vm)
            valores+=vm[0]
    util_res = [0 for i in cloudlets[0]]
    for cloud in utilizado:
        util_res = sum_lists(util_res,cloud)
    return (valores, util_res, vms)



def alloc(dataset, cloudlets_data):
    items = []
    for vm in dataset:
        items.append( (vm[0],tuple(vm)) )
    cloudlets = tuple([tuple(cloudlet) for cloudlet in cloudlets_data])

    total_cpu = 0
    for cloudlet in cloudlets:
        total_cpu += cloudlet[0]
    ct = time.time()
    value, weights, vms = preenche(cloudlets, items)
    exec_time=time.time()-ct
    return {"fraction": value/total_cpu, "time": exec_time, "vm_ammount": len(vms)}
