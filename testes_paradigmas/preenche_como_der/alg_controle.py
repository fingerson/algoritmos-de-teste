import time

def le_lists(a,b):
    ist = True
    for i in range(min(len(a),len(b))):
        ist = ist and (a[i] <= b[i])
    return ist

def sum_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] + b[i])
    return c

def preenche_linear(cloudlet, items):
    vms = []
    valores = 0
    utilizado = [0]*len(cloudlet)
    for item in items:
        temp = sum_lists(utilizado,item[1])
        if le_lists(temp,cloudlet):
            utilizado = temp
            vms.append(item)
            valores+=item[0]
    return (valores, utilizado, vms)



def alloc(dataset, cloudlets_data, steps = 20):
    items = []
    for vm in dataset:
        items.append( (vm[0],tuple(vm)) )
    cloudlets = tuple([tuple(cloudlet) for cloudlet in cloudlets_data])

    total_cpu = 0
    for cloudlet in cloudlets:
        total_cpu += cloudlet[0]
    total_value = 0
    vm_alloc_list = []
    ct = time.time()
    for cloudlet in cloudlets:
        value, weights, vms = preenche_linear(cloudlet, items)
        total_value += value
        vm_alloc_list += vms
        for vm in vms:
            items.remove(vm)
    exec_time=time.time()-ct
    return {"fraction": total_value/total_cpu, "time": exec_time, "vm_ammount": len(vm_alloc_list)}
