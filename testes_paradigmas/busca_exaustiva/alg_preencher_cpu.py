import Cloudlet
import VM
import dataset_generator
import random
import time

def get_random(min, max):
    frac = random.random()
    frac = frac*(max-min) + min
    return frac

def get_dataset(n = 1000):
    return dataset_generator.new_set(400,4000,0.5,4,5,50,n)

def get_cloudlet_data():
    return [get_random(30000,50000),get_random(20,80),get_random(1000,10000)]

def exhaustive_search(cloudlets, vm_list):
    if len(vm_list) == 0:
        total_cpu = 0
        available_cpu = 0
        vm_ammount = 0
        for cloud in cloudlets:
            vm_ammount += cloud.vm_ammount
            total_cpu += cloud.max_cpu
            available_cpu += cloud.cpu
        #print("maior: "+str(1 - available_cpu/total_cpu))
        return [1 - available_cpu/total_cpu, vm_ammount]
    current_vm = vm_list.pop()
    total_cpu = 0
    available_cpu = 0
    vm_ammount = 0
    for cloud in cloudlets:
        vm_ammount += cloud.vm_ammount
        total_cpu += cloud.max_cpu
        available_cpu += cloud.cpu
    largest_fraction = 1 - available_cpu/total_cpu
    for cloud in cloudlets:
        if cloud.add_vm(current_vm):
            ret_data = exhaustive_search(cloudlets, vm_list)
            if ret_data[0] > largest_fraction:
                largest_fraction = ret_data[0]
                vm_ammount = ret_data[1]
            if cloud.remove_vm(current_vm) == False:
                raise Exception('Missing VM in Cloudlet')
    ret_data = exhaustive_search(cloudlets, vm_list)
    if ret_data[0] > largest_fraction:
        largest_fraction = ret_data[0]
        vm_ammount = ret_data[1]
    vm_list.insert(0,current_vm)
    #print("maior: "+str(largest_fraction))
    return [largest_fraction, vm_ammount]

def alloc(dataset, cloudlets_data):
    vm_list = VM.build_vms(dataset)
    cloudlets = [Cloudlet.Cloudlet(cloudlet_data[0],cloudlet_data[1], cloudlet_data[2]) for cloudlet_data in cloudlets_data]
    ct = time.time()
    fraction, vm_ammount = exhaustive_search(cloudlets,vm_list)
    exec_time=time.time()-ct
    return {"fraction": fraction, "time": exec_time, "vm_ammount": vm_ammount}

if __name__ == '__main__':
    n_cloud = int(input("Digite o numero de cloudlets"))
    cloudlet_data = []
    for i in range(n_cloud):
        cloudlet_data.append([int (j) for j in input("Digite os parametros da cloudlet " +str(i+1)+ "(cpu ram hd)").split()])
    n_vm = int(input("Digite o numero de vms"))
    vm_data = []
    for i in range(n_vm):
        vm_data.append([int (j) for j in input("Digite os parametros da vm " +str(i+1)+ "(cpu ram hd)").split()])
    #print(cloudlet_data)
    #print(vm_data)
    print(alloc(vm_data, cloudlet_data))
