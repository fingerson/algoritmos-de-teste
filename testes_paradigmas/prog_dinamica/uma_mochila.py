import math

def sum_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] + b[i])
    return c

def sub_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] - b[i])
    return c

def le_lists(a,b):
    ist = True
    for i in range(min(len(a),len(b))):
        ist = ist and (a[i] <= b[i])
    return ist

def div_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        if b[i] != 0 :
            c.append(a[i]/b[i])
        else:
            c.append(0)
    return c

def mult_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i]*b[i])
    return c

def la_lists(a,b):
    if sum(a) < sum(b):
        return True
    return False


def multi_knapsack(limit, items, step_count):


    nstep = (step_count+1)**len(limit)

    values = [0]*(nstep)
    weights = []
    end_items = []
    for i in range(nstep):
        a = [0]*len(limit)
        b = []
        end_items.append(b)
        weights.append(a)



    steps = [i/(step_count) for i in limit]
    list_to_index = [1]
    for i in range(len(limit)-1):
        list_to_index = [list_to_index[0]*step_count]+list_to_index

    for i in range(len(items)):
        n_values = []
        n_weights = []
        n_end_items = []

        current_bag = [0 for i in limit]
        current_counter = [0 for i in limit]
        for j in range(nstep):
            if not le_lists(items[i][1],current_bag):
                n_weights.append(weights[j])
                n_values.append(values[j])
                n_end_items.append(end_items[j])
            elif le_lists(sum_lists(items[i][1],weights[j]),current_bag):
                n_weights.append(sum_lists(items[i][1],weights[j]))
                n_values.append(items[i][0] + values[j])
                n_end_items.append(end_items[j] + [items[i]])
            else:
                best_w = weights[j]
                best_v = values[j]
                best_items = end_items[j]
                index = j-math.ceil(sum(mult_lists(div_lists(items[i][1],steps),list_to_index)))
                if items[i][0] + values[index] > values[j] and le_lists(sum_lists(items[i][1],weights[index]), current_bag):
                    best_v = items[i][0] + values[index]
                    best_w = sum_lists(items[i][1],weights[index])
                    best_items = end_items[index] + [items[i]]
                n_weights.append(best_w)
                n_values.append(best_v)
                n_end_items.append(best_items)
            current_counter[-1]+=1
            for k in range(len(current_counter)-1, 0 , -1):
                if current_counter[k] > step_count:
                    current_counter[k] = 0
                    current_counter[k-1] += 1

            current_bag = mult_lists(current_counter, steps)
        values = n_values
        weights = n_weights
        end_items = n_end_items
    return (values[-1], weights[-1], end_items[-1])


if __name__ == "__main__":
    limit = [int(i) for i in input().split()]
    items = []
    inpt = [int(i) for i in input().split()]
    while inpt[0] > 0:
        inpt = [inpt.pop(0), inpt]
        print(inpt)
        items.append(inpt)
        inpt = [int(i) for i in input().split()]

    step_count = int(input())
    values, weights, end_items = multi_knapsack(limit, items, step_count)
    print(values)
    print(weights)
    print(end_items)
    for i in end_items:
        items.remove(i)
    print(items)
