import testes_paradigmas.prog_dinamica.uma_mochila as uma_mochila
import time

def alloc(dataset, cloudlets_data, steps = 20):
    items = []
    for vm in dataset:
        items.append( (vm[0],tuple(vm)) )
    cloudlets = tuple([tuple(cloudlet) for cloudlet in cloudlets_data])

    total_cpu = 0
    for cloudlet in cloudlets:
        total_cpu += cloudlet[0]
    total_value = 0
    vm_alloc_list = []
    ct = time.time()
    for cloudlet in cloudlets:
        value, weights, vms = uma_mochila.multi_knapsack(cloudlet, items, steps)
        total_value += value
        vm_alloc_list += vms
        for vm in vms:
            items.remove(vm)
    exec_time=time.time()-ct
    return {"fraction": total_value/total_cpu, "time": exec_time, "vm_ammount": len(vm_alloc_list)}
