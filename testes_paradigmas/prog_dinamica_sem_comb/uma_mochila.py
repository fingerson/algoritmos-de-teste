import math

def sum_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] + b[i])
    return c

def sub_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i] - b[i])
    return c

def le_lists(a,b):
    ist = True
    for i in range(min(len(a),len(b))):
        ist = ist and (a[i] <= b[i])
    return ist

def div_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        if b[i] != 0 :
            c.append(a[i]/b[i])
        else:
            c.append(0)
    return c

def mult_lists(a,b):
    c = []
    for i in range(min(len(a),len(b))):
        c.append(a[i]*b[i])
    return c

def la_lists(a,b):
    if sum(a) < sum(b):
        return True
    return False


def multi_knapsack(limit, items, nstep):
    limit = [i+1 for i in limit]
    values = [0]*(nstep+1)
    weights = []
    end_items = []
    for i in range(nstep+1):
        a = [0]*len(limit)
        b = []
        end_items.append(b)
        weights.append(a)

    steps = [i/nstep for i in limit]

    for i in range(len(items)):
        n_values = []
        n_weights = []
        n_end_items = []
        current_bag = [0,0,0]
        for j in range(nstep+1):
            if not le_lists(items[i][1],current_bag):
                n_weights.append(weights[j])
                n_values.append(values[j])
                n_end_items.append(end_items[j])
            elif le_lists(sum_lists(items[i][1],weights[j]),current_bag):
                n_weights.append(sum_lists(items[i][1],weights[j]))
                n_values.append(items[i][0] + values[j])
                n_end_items.append(end_items[j] + [items[i]])
            else:
                best_w = weights[j]
                best_v = values[j]
                best_items = end_items[j]
                index_list = [math.floor(l) for l in div_lists(sub_lists(current_bag, items[i][1]),steps)]
                index_list = index_list + [k+1 for k in index_list]
                for k in range(len(index_list)):
                    possible_sum = sum_lists(weights[index_list[k]],items[i][1])
                    if le_lists(possible_sum, current_bag):
                        if values[index_list[k]] + items[i][0] > best_v:
                            best_v = values[index_list[k]] + items[i][0]
                            best_w = possible_sum
                            best_items = end_items[index_list[k]] + [items[i]]
                        elif values[index_list[k]] + items[i][0] == best_v:
                            if la_lists(possible_sum,best_w):
                                best_v = values[index_list[k]] + items[i][0]
                                best_w = possible_sum
                                best_items = end_items[index_list[k]] + [items[i]]
                n_weights.append(best_w)
                n_values.append(best_v)
                n_end_items.append(best_items)
            current_bag = sum_lists(current_bag, steps)
            if not le_lists(current_bag,limit):
                break
        values = n_values
        weights = n_weights
        end_items = n_end_items
    return (values[-1], weights[-1], end_items[-1])


if __name__ == "__main__":
    limit = [int(i) for i in input().split()]
    items = []
    inpt = [int(i) for i in input().split()]
    while inpt[0] > 0:
        inpt = [inpt.pop(0), inpt]
        print(inpt)
        items.append(inpt)
        inpt = [int(i) for i in input().split()]

    step_count = int(input())
    values, weights, end_items = multi_knapsack(limit, items, step_count)
    print(values)
    print(weights)
    print(end_items)
    for i in end_items:
        items.remove(i)
    print(items)
