import numpy
import math


def get_normal_list(minv,maxv, number, desvio):
    avg = (minv + maxv)/2
    sigma = abs(avg-minv)* desvio
    return numpy.random.normal(avg,sigma,number)

def get_normal_log_list(minv,maxv, number, desvio):
    avg = (math.log(minv) + math.log(maxv))/2
    sigma = abs(avg-math.log(minv))* desvio
    return list(map(math.exp, numpy.random.normal(avg,sigma,number)))

def get_uniform_list(minv,maxv, number):
    return numpy.random.uniform(minv, maxv ,number)

def intv(minv, maxv, val):
    if minv > maxv:
        maxv,minv = minv,maxv
    return min(maxv,max(minv,val))

def get_random_cloudlet(cloudlet_data,type = 0):
    ret_val = []
    for i in range(3):
        if type == 0:
            value = get_normal_list(cloudlet_data[i],cloudlet_data[i+3],1,1.0/3.0)
        elif type == 1:
            value = get_normal_log_list(cloudlet_data[i],cloudlet_data[i+3],1,1.0/3.0)
        else:
            value = get_uniform_list(cloudlet_data[i],cloudlet_data[i+3],1)
        ret_val.append(intv(cloudlet_data[i],cloudlet_data[i+3],value[0]))
    return ret_val


def new_set(cpumin,cpumax,rammin,rammax,hdmin,hdmax,number, tipo = 2):
    ret = []
    desvio = 1.0/3.0
    if tipo == 0:
        cpus = get_normal_list(cpumin,cpumax,number, desvio)
        rams = get_normal_list(rammin,rammax,number, desvio)
        hds  = get_normal_list(hdmin,hdmax,number, desvio)
    elif tipo == 1:
        cpus = get_normal_log_list(cpumin,cpumax,number, desvio)
        rams = get_normal_log_list(rammin,rammax,number, desvio)
        hds  = get_normal_log_list(hdmin,hdmax,number, desvio)
    else:
        cpus = get_uniform_list(cpumin,cpumax,number)
        rams = get_uniform_list(rammin,rammax,number)
        hds  = get_uniform_list(hdmin,hdmax,number)
    for i in range(number):
        ret.append([intv(cpumin,cpumax,cpus[i]),intv(rammin,rammax,rams[i]),intv(hdmin,hdmax,hds[i])])
    return ret

if __name__ == '__main__':
    inpt = [int(i) for i in input("Digite os limites inferiores e superiores de CPU: ").split()]
    cpumin = inpt[0]
    cpumax = inpt[1]
    if cpumin > cpumax:
        cpumin,cpumax = cpumax,cpumin
    inpt = [int(i) for i in input("Digite os limites inferiores e superiores de RAM: ").split()]
    rammin = inpt[0]
    rammax = inpt[1]
    if rammin > rammax:
        rammin,rammax = rammax,rammin
    inpt = [int(i) for i in input("Digite os limites inferiores e superiores de Armazenamento: ").split()]
    hdmin = inpt[0]
    hdmax = inpt[1]
    if hdmin > hdmax:
        hdmin,hdmax = hdmax,hdmin
    desvio = float(input("Digite a  razão do desvio padrão (>0, padrão é 0.333) "))
    quantidade = int(input("Digite o número de maquinas virtuais que você deseja: "))
    set = new_set(cpumin,cpumax,rammin,rammax,hdmin,hdmax,desvio,quantidade)
